<h1 align="center">
  Ricette della nonna Luciana
</h1>

Luciana, mia nonna materna, era molto brava a cucinare; le piaceva sperimentare nuove ricette, viste in televisione o lette sui suoi libri di cucina.

Purtroppo, la nonna Luciana ci ha lasciati il 9 gennaio 2023.

Questo repository raccoglie le ricette che mia nonna ci ha lasciato, scritte a mano sui suoi quaderni di appunti o tramandate a voce.

Dedico questa raccolta in formato digitale a mio nonno, che con mia nonna ha condiviso ben 54 anni di matrimonio, fino all'ultimo giorno insieme, e che ora soffre per la mancanza della persona a lui più cara.

<br>

<p align="right"><i>Luca, 26/02/2023</i></p>
