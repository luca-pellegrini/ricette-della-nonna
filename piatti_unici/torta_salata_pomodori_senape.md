# Torta salata con pomodorini e senape

Cottura in forno: **30 min**

## Ingredienti

* Pasta sfoglia (se comprata al supermercato, forma rotonda)
* Panna da cucina (2 cucchiai, circa un barattolo da 125 ml)
* Senape (2 cucchiai)
* Pomodori piccoli
* Pane grattugiato
* Olio
* Origano
* Formaggio grattugiato (facoltativo)

## Preparazione

Si srotola la pasta sfoglia e la si dispone in uno stampo circolare metallico (per la cottura in forno).
Con una forchetta si fanno alcuni buchi nella sfoglia.

Si mescolano panna e senape insieme.

Si spalma il miscuglio di panna e senape sulla sfoglia nello stampo, in uno strato uniforme e sottile.

Dopo averli lavali, si tagliano a metà i pomodorini e li si dispone sullo strato di panna e senape, con il taglio rivolto verso l'alto.

Poi si cosparge il tutto con il pane grattugiato: 2/3 cucchiai (quanto basta); se si mette troppo pane, la torta rischia di asciugarsi troppo velocemente durante la cottura.

Infine, si cosparge con origano, un pizzico di sale, un filo d'olio, formaggio grattugiato (quest'ultimo opzionale).

Si cuoce in forno preriscaldato a 200°C per circa mezz'ora. Controllare di tanto in tanto, che non si asciughi troppo.
